package com.lrdoz.roti.ws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RotiWsApplication {

	public static void main(String[] args) {
		SpringApplication.run(RotiWsApplication.class, args);
	}

}
